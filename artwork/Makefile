CUR_DIR?=$(notdir $(shell pwd))
OUT_DIR?=export

IN_SVGS?=$(wildcard *.inkscape.svg)
_OUT_BASEPATHS?=$(addprefix $(OUT_DIR)/,$(IN_SVGS:.inkscape.svg=))

SVGS?=$(addsuffix .svg,$(_OUT_BASEPATHS))
PDFS?=$(addsuffix .pdf,$(_OUT_BASEPATHS))

# export heights for raster graphics
#                         SVGA HD  f/HD 4k   8k
HEIGHTS?=16 32 64 128 512 600  720 1080 2160 4320

PNGS?=$(foreach height,$(HEIGHTS),\
	$(addsuffix -$(height)p.png,$(_OUT_BASEPATHS)))

COMMON_EXPORT_OPTS=\
	--export-overwrite \
	--export-area-page \
	--export-text-to-path \
	--export-filename $@ \
	$<

all: svg pdf png

svg: $(SVGS)
pdf: $(PDFS)
png: $(PNGS)

$(OUT_DIR):
	mkdir -p $(OUT_DIR)

$(OUT_DIR)/%.svg: %.inkscape.svg | $(OUT_DIR)
	inkscape $(COMMON_EXPORT_OPTS) --export-plain-svg --vacuum-defs

$(OUT_DIR)/%.pdf: %.inkscape.svg | $(OUT_DIR)
	inkscape $(COMMON_EXPORT_OPTS) --export-type=pdf

define PNG_template =
$(OUT_DIR)/%-$(1)p.png: %.inkscape.svg | $(OUT_DIR)
	inkscape $$(COMMON_EXPORT_OPTS) --export-type=png --export-height=$(1)
endef
$(foreach height,$(HEIGHTS),$(eval $(call PNG_template,$(height))))

clean:
	rm -f $(SVGS) $(PDFS) $(PNGS)
	rmdir $(OUT_DIR)

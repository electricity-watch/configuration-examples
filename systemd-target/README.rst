With the files in this directory, the current regional rating of
`electricity.watch <https://electricity.watch>`__ can be reflected in
systemd. As a result, services be controlled accordingly, e.g., only
run when consuming electricity is sustainable.

In detail, this works as follows. A systemd timer
(``electricity-watch-check.timer``) will activate a systemd service
(``electricity-watch-check.service``) to check the `electricity.watch
<https://electricity.watch>`__ API. In dependence on the response, the
service will activate or deactivate a systemd target
(``electricity-watch-not-bad.target``) which reflects the
regional electricity rating.

To set up all the units mentioned above, ``make install`` can be used.
Alternatively, all units can be enabled and started manually, just like
``make install`` would do it (the Makefile is quite straightforward).
To uninstall, ``make uninstall`` is provided.

Services which should run in dependence
on the regional electricity rating can then bind to this target:

.. code:: ini

  [Unit]
  # responsible for stopping with the target
  BindsTo=electricity-watch-not-bad.target

  [Service]
  …

  [Install]
  # responsible for starting with the target
  WantedBy=electricity-watch-not-bad.target

Because of the modified ``[Install]`` section,
``systemctl reenable the-modified.service`` is required.

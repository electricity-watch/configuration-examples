In this repository, examples are collected for configuring operating
systems, services, tools, etc., to make use of `electricity.watch
<https://electricity.watch>`__ and thus consume electricity more
sustainably.
